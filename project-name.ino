int pin = 4;
int time = 1000;

void setup(){
  pinMode(pin, OUTPUT);
}

void loop(){
  digitalWrite(pin, HIGH);
  delay(time);
  digitalWrite(pin, LOW);
  delay(time);
}
